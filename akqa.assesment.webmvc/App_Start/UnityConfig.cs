using akqa.assesment.webmvc.Classes.Concrete;
using akqa.assesment.webmvc.Inferfaces;
using akqa.assesment.webmvc.Models;
using System;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;

namespace akqa.assesment.webmvc
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();

              //DependencyResolver.SetResolver(new UnityDependencyResolver(container));

              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // Register your type's mappings 
            container.RegisterType<ISettings, AppSettings>();
            container.RegisterType<IPaymentRequest, PaymentRequest>();
            container.RegisterType<IPaymentRepository, PaymentRepository>();
            
        }
    }
}
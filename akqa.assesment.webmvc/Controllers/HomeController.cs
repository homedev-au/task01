﻿using akqa.assesment.webmvc.Inferfaces;
using akqa.assesment.webmvc.Models;
using akqa.assesment.webmvc.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace akqa.assesment.webmvc.Controllers
{
    public class HomeController : Controller
    {

        private IPaymentRepository _paymentRepository;

        public HomeController(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(PaymentViewModel model) 
        {
            if (TryValidateModel(model))
            {
                using (var service = new PaymentService.PaymentServiceClient())
                {
                    // Save the payment
                    model.PaymentResponse = _paymentRepository.SavePayment(model.PaymentRequest, service) as PaymentResponse;
                    return View(model);
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "The Cheque Printing Application";

            return View();
        }
    }
}
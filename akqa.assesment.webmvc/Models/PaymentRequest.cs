﻿using akqa.assesment.webmvc.Inferfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace akqa.assesment.webmvc.Models
{
    public class PaymentRequest : IPaymentRequest
    {
        [Required(ErrorMessage = "First Name is a required field")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is a required field")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Amount is a required field")]
        public double PaymentAmount { get; set; }
        
    }
}
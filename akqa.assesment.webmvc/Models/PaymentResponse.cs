﻿using akqa.assesment.webmvc.Inferfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace akqa.assesment.webmvc.Models
{
    public class PaymentResponse : PaymentRequest, IPaymentResponse
    {
        public string PersonDisplayName
        {
            get
            {
                return $"{this.FirstName} {this.LastName}";
            }
        }
        public string PaymentDisplayValue { get; set; }
    }
}
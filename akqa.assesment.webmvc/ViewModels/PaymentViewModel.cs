﻿using akqa.assesment.webmvc.Inferfaces;
using akqa.assesment.webmvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace akqa.assesment.webmvc.ViewModels
{
    public class PaymentViewModel
    {
        public PaymentRequest PaymentRequest { get; set; }

        public PaymentResponse PaymentResponse { get; set; }

        public bool IsProcessed
        {
            get
            {
                return (PaymentResponse != null);
            }
        }
    }
}
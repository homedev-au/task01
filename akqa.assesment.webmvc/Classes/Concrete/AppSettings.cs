﻿using akqa.assesment.business;
using akqa.assesment.webmvc.Inferfaces;
using akqa.assesment.webmvc.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace akqa.assesment.webmvc.Classes.Concrete
{
    public class AppSettings : ISettings
    {
        public TextFormat CheckOutputFormat
        {
            get
            {
                return Settings.Default.CheckOutputFormat;
            }
        }
    }
}
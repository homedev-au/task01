﻿using akqa.assesment.webmvc.Inferfaces;
using akqa.assesment.webmvc.Models;
using akqa.assesment.webmvc.PaymentService;

namespace akqa.assesment.webmvc.Classes.Concrete
{
    public class PaymentRepository : IPaymentRepository
    {
        private ISettings _settings;
    
        public PaymentRepository(ISettings settings)
        {
            _settings = settings;
        }

        public IPaymentResponse SavePayment(IPaymentRequest payment, IPaymentService service)
        {
            
                var paymentResult = service.SavePayment(payment.FirstName, payment.LastName, payment.PaymentAmount, _settings.CheckOutputFormat);

                PaymentResponse payementResponse = new PaymentResponse
                {
                    FirstName = paymentResult.FirstName,
                    LastName = paymentResult.LastName,
                    PaymentAmount = paymentResult.PaymentAmount,
                    PaymentDisplayValue = paymentResult.PaymentDisplayValue
                };

                return payementResponse;
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.webmvc.Inferfaces
{
    public interface IPaymentResponse : IPaymentRequest
    {
        string PersonDisplayName { get; }
        string PaymentDisplayValue { get; set; }
    }
}

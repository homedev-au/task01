﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.webmvc.Inferfaces
{
    public interface IPaymentRequest
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        double PaymentAmount { get; set; }
       
    }
}

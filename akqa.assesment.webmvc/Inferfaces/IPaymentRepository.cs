﻿using akqa.assesment.webmvc.PaymentService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.webmvc.Inferfaces
{
    public interface IPaymentRepository
    {
        IPaymentResponse SavePayment(IPaymentRequest payment, IPaymentService service);

    }
}

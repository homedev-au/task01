# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Personal Assessment task

### Technology Platform ###

	* Solution was created on Visual Studio 2017 with .net 4.7.
	* You need the .net 4.7 targeting pack to open. Targeting packs available here: https://www.microsoft.com/net/download/visual-studio-sdks
	* Testing via MS Test 
	* used Unity DI framework
	* EntityFramework for Data Layer 
	* Using localDB which should be on most VS computers, it is installed by default in 2017 by choosing the ".NET Desktop Development" workload.
	* All packages used in the application can be restored by nuGet.

### Solution Overview ###

	* I took the requirements and decided that this would be a system to generate the details of cheques.
	* The example accepted decimals, so to write that to text it needed to output a textual separator between the whole numbers and decimal component, so rather than outputing 
		* One point Five; the solution produces
		* One Dollar and Five Cents
	
### Architectural Decisions ###

	* Created a business library to house the key NumberToText Conversion logic.
		* This library could be re-usable in other applications.
		* Also able to be brought into unit testing projects
		
	* Service is created as a WCF Service
		* Exposes a single method  "SavePayment" which accepts both the name and the amount details.
		* Name split into FirstName / LastName to give UI designer flexibility on how to render the name. Also when persisted to DB gives more options for searching and sorting.
		* The idea for accepting all values is the assumption was that the system would persist this information.
		* It also encapsulates the generation of the Currency Display Value
		* There is an overload that allows the developer to choose the output format, lower case, upper case or title case
		* This has been exposed all the way through to the MVC application as a setting
		
	* EntityFramework 
		* code first implementation against localDb
		* persists the API call  to the DB as part of the logging of the service call.
		
	* Web - Asp.net MVC
		* Using native razor views
		* Unity Dependency Injection
		* AppSettings/ISettings interface to allows the UI to pass through to the Service call and controls the format of the text for the application



### How do I get set up? ###

* Restore Nuget Packages
* Ensure web application set as startup project
* Run

### Contribution guidelines ###

* Please don't contriubute

### Who do I talk to? ###

* John Crawford

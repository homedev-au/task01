﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using akqa.assesment.webmvc;
using akqa.assesment.webmvc.Controllers;
using akqa.assesment.webmvc.Inferfaces;
using Unity;
using akqa.assesment.webmvc.test.Concrete;

namespace akqa.assesment.webmvc.test.Controllers
{
    
    [TestClass]
    public class HomeControllerTest
    {
        internal static IUnityContainer _unityContainer;
        internal static IPaymentRepository _paymentRepository;

        [ClassInitialize]
        public static void Initialise(TestContext testContext)
        {

            _unityContainer = TestBootstrapper.Register();

            _paymentRepository = _unityContainer.Resolve<IPaymentRepository>();

        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController(_paymentRepository);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController(_paymentRepository);

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("The Cheque Printing Application", result.ViewBag.Message);
            Assert.IsNotNull(result);
            
        }
        
    }
}

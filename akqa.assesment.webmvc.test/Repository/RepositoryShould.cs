﻿using System;
using akqa.assesment.webmvc.Inferfaces;
using akqa.assesment.webmvc.Models;
using akqa.assesment.webmvc.PaymentService;
using akqa.assesment.webmvc.test.Concrete;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unity;

namespace akqa.assesment.webmvc.test.Repository
{
    [TestClass]
    public class RepositoryShould
    {

        internal static IUnityContainer _unityContainer;
        internal static IPaymentRepository _paymentRepository;
        internal static IPaymentService _service;

        [ClassInitialize]
        public static void Initialise(TestContext testContext)
        {

            _unityContainer = TestBootstrapper.Register();

            _paymentRepository = _unityContainer.Resolve<IPaymentRepository>();
            _service = new MockPaymentService();

        }

        [TestMethod]
        public void SavePayment()
        {
            var paymentRequest = new PaymentRequest()
            {
                FirstName = "John",
                LastName = "Crawford",
                PaymentAmount = 123.89
            };

            var response = _paymentRepository.SavePayment(paymentRequest, _service);

            Assert.AreEqual("One Hundred and Twenty-Three Dollars and Eighty-Nine Cents", response.PaymentDisplayValue);

        }
    }
}

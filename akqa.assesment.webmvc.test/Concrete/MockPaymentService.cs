﻿using akqa.assesment.business;
using akqa.assesment.business.Utilities;
using akqa.assesment.webmvc.PaymentService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.webmvc.test.Concrete
{
    public class MockPaymentService : IPaymentService
    {
        public Payment Payment { get; private set; }

        public Payment SavePayment(string firstName, string lastName, double paymentAmount, TextFormat textFormat = TextFormat.TitleCase)
        {
            Payment payment = new Payment()
            {
                FirstName = firstName,
                LastName = lastName,
                PaymentAmount = paymentAmount
            };

            payment.PaymentDisplayValue = NumberUtility.ConvertAmountToCurrencyText(paymentAmount, textFormat);

            return payment;
        }

        public Task<Payment> SavePaymentAsync(string firstName, string lastName, double paymentAmount, TextFormat textFormat = TextFormat.TitleCase)
        {
            Payment payment = new Payment()
            {
                FirstName = firstName,
                LastName = lastName,
                PaymentAmount = paymentAmount
            };

            payment.PaymentDisplayValue = NumberUtility.ConvertAmountToCurrencyText(paymentAmount, textFormat);

            return Task.FromResult(payment);
        }
    }
}

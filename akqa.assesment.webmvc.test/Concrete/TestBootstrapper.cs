﻿using akqa.assesment.webmvc.Classes.Concrete;
using akqa.assesment.webmvc.Inferfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace akqa.assesment.webmvc.test.Concrete
{
    public class TestBootstrapper
    {
        public static IUnityContainer Register()
        {

            IUnityContainer container = new UnityContainer();

            RegisterTypes(container);

            return container;
        }

        private static void RegisterTypes(IUnityContainer container)
        {

            container.RegisterType<IPaymentRepository, PaymentRepository>();
            container.RegisterType<ISettings, MockAppSettings>();

        }
    }
}

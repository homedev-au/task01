﻿using akqa.assesment.business;
using akqa.assesment.webmvc.Inferfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.webmvc.test.Concrete
{
    /// <summary>
    /// Need to control the output format in the test
    /// regardless of what is in the settings of the web application
    /// </summary>
    public class MockAppSettings : ISettings
    {
        public TextFormat CheckOutputFormat => TextFormat.TitleCase;
    }
}

﻿using akqa.assesment.business;
using akqa.assesment.business.Data;
using System;

namespace akqa.assesment.paymentservice
{
    public class PaymentService : IPaymentService
    {
        public Payment SavePayment(string firstName, string lastName, double paymentAmount, TextFormat textFormat = TextFormat.TitleCase)
        {
            // TODO: persist to back end data storage
            // SQL server entity framework model

            if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(lastName))
            {
                string errorMsg = "Invalid Parameters - " +
                    (string.IsNullOrWhiteSpace(firstName) ? ", A FirstName is required" : "") +
                    (string.IsNullOrWhiteSpace(lastName) ? ", A LastName is required" : "");

                throw new InvalidOperationException(errorMsg);
            }

            // Persist to sql
            int id = AkqaRepository.InsertPayment(firstName, lastName, paymentAmount);

            // Id is the identity of the inserted record

            // return the populated Payment Object
            return PaymentFactory.CreatePayment(firstName, lastName, paymentAmount, textFormat);
        }
    }
}
﻿using akqa.assesment.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace akqa.assesment.paymentservice
{
    [ServiceContract]
    public interface IPaymentService
    {

        [OperationContract]
        Payment SavePayment(string firstName, string lastName, double paymentAmount, TextFormat textFormat = TextFormat.TitleCase);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Payment
    {
        private string _firstName;
        private string _lastName;
        private double _paymentAmount;
        private string _paymentDisplayValue;

        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        [DataMember]
        public double PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        [DataMember]
        public string PaymentDisplayValue
        {
            get { return _paymentDisplayValue; }
            set { _paymentDisplayValue = value; }
        }
    }
}

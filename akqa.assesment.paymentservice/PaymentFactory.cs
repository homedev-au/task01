﻿using akqa.assesment.business;
using akqa.assesment.business.Utilities;

namespace akqa.assesment.paymentservice
{
    public static class PaymentFactory
    {
        /// <summary>
        /// Helper class to encapsulate the logic of setting the dependent payment text property
        /// Did not want to put a contructor on the contract class
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="paymentAmount"></param>
        /// <returns></returns>
        public static Payment CreatePayment(string firstName, string lastName, double paymentAmount, TextFormat textFormat = TextFormat.TitleCase)
        {
            Payment payment = new Payment()
            {
                FirstName = firstName,
                LastName = lastName,
                PaymentAmount = paymentAmount
            };

            // set the payment text value
            payment.PaymentDisplayValue = NumberUtility.ConvertAmountToCurrencyText(payment.PaymentAmount, textFormat);
            
            return payment;
        }
    }
}
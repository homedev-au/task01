﻿using System;
using akqa.assesment.business;
using akqa.assesment.business.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace akqua.assesment.test
{
    [TestClass]
    public class ConvertAmountToCurrencyTextShould
    {
        [TestMethod]
        public void HandleZero()
        {
            Assert.AreEqual("Zero Dollars and Zero Cents", NumberUtility.ConvertAmountToCurrencyText(0));
        }

        [TestMethod]
        public void HandleCentsOnly()
        {
            Assert.AreEqual("Zero Dollars and One Cent", NumberUtility.ConvertAmountToCurrencyText(0.01));
            Assert.AreEqual("Zero Dollars and Ninety-Nine Cents", NumberUtility.ConvertAmountToCurrencyText(0.99));
        }

        [TestMethod]
        public void HandleDollarsOnly()
        {
            Assert.AreEqual("One Hundred Dollars and Zero Cents", NumberUtility.ConvertAmountToCurrencyText(100));
            Assert.AreEqual("One Thousand Dollars and Zero Cents", NumberUtility.ConvertAmountToCurrencyText(1000));
        }

        [TestMethod]
        public void HandleDollarsAndCents()
        {
            Assert.AreEqual("One Dollar and Twenty-Three Cents", NumberUtility.ConvertAmountToCurrencyText(1.23));
            Assert.AreEqual("One Thousand Dollars and Zero Cents", NumberUtility.ConvertAmountToCurrencyText(1000));
        }

        [TestMethod]
        public void HandleTextFormats()
        {
            Assert.AreEqual("one hundred dollars and zero cents", NumberUtility.ConvertAmountToCurrencyText(100, TextFormat.Lowercase));
            Assert.AreEqual("ONE THOUSAND DOLLARS AND ZERO CENTS", NumberUtility.ConvertAmountToCurrencyText(1000, TextFormat.Uppercase));
        }

        [TestMethod]
        public void HandleNegativeValue()
        {
            Assert.AreEqual("(Minus) One Hundred and Twenty-Three Dollars and Fourty-Two Cents", NumberUtility.ConvertAmountToCurrencyText(-123.42));
        }
    }
}

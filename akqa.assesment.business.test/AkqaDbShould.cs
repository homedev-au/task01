﻿using System;
using akqa.assesment.business.Data;
using akqa.assesment.business.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace akqa.assesment.business.test
{
    [TestClass]
    public class AkqaDbShould
    {
        [TestMethod]
        public void SaveANewPayment()
        {
            int rowCount = AkqaRepository.GetNumberOfPayments();

            int id = AkqaRepository.InsertPayment("Luke", "Skywalker", 345.67);

            int newCount = AkqaRepository.GetNumberOfPayments();


            Assert.IsTrue(id > 0, "No Identity returned when creating Payment");
            Assert.IsTrue(newCount == rowCount + 1, "Did not insert a new row");
        }
    }
}

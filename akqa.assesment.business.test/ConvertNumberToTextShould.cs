﻿using System;
using akqa.assesment.business;
using akqa.assesment.business.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace akqua.assesment.test
{
    [TestClass]
    public class ConvertNumberToTextShould
    {
        [TestMethod]
        public void ReturnZero()
        {
            string result = NumberUtility.ConvertNumberToText(0);
            Assert.AreEqual("Zero", result);
        }

        [TestMethod]
        public void ReturnSingleDigitNumbers()
        {
            string result = string.Empty;
            for(int i=0; i<10; i++)
            {
                result = NumberUtility.ConvertNumberToText(i);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual("Zero", result);
                        break;
                    case 1:
                        Assert.AreEqual("One", result);
                        break;
                    case 2:
                        Assert.AreEqual("Two", result);
                        break;
                    case 3:
                        Assert.AreEqual("Three", result);
                        break;
                    case 4:
                        Assert.AreEqual("Four", result);
                        break;
                    case 5:
                        Assert.AreEqual("Five", result);
                        break;
                    case 6:
                        Assert.AreEqual("Six", result);
                        break;
                    case 7:
                        Assert.AreEqual("Seven", result);
                        break;
                    case 8:
                        Assert.AreEqual("Eight", result);
                        break;
                    case 9:
                        Assert.AreEqual("Nine", result);
                        break;

                }
            }
            
        }

        [TestMethod]
        public void ReturnTeenNumbers()
        {
            string result = string.Empty;
            for (int i = 10; i < 20; i++)
            {
                result = NumberUtility.ConvertNumberToText(i);

                switch (i)
                {
                    case 10:
                        Assert.AreEqual("Ten", result);
                        break;
                    case 11:
                        Assert.AreEqual("Eleven", result);
                        break;
                    case 12:
                        Assert.AreEqual("Twelve", result);
                        break;
                    case 13:
                        Assert.AreEqual("Thirteen", result);
                        break;
                    case 14:
                        Assert.AreEqual("Fourteen", result);
                        break;
                    case 15:
                        Assert.AreEqual("Fifteen", result);
                        break;
                    case 16:
                        Assert.AreEqual("Sixteen", result);
                        break;
                    case 17:
                        Assert.AreEqual("Seventeen", result);
                        break;
                    case 18:
                        Assert.AreEqual("Eighteen", result);
                        break;
                    case 19:
                        Assert.AreEqual("Nineteen", result);
                        break;

                }
            }

        }

        [TestMethod]
        public void HandleNegativeNumbers()
        {
            Assert.IsTrue(NumberUtility.ConvertNumberToText(-23).StartsWith("(Minus)"), "Negative number does not start with (Minus)");
        }

        [TestMethod]
        public void HandleLargeWholeNumbers()
        {
            Assert.AreEqual("One Thousand", NumberUtility.ConvertNumberToText(1000));
            Assert.AreEqual("One Million", NumberUtility.ConvertNumberToText(1000000));
            Assert.AreEqual("One Billion", NumberUtility.ConvertNumberToText(1000000000));
        }

        [TestMethod]
        public void HandleBillions()
        {
            string expectedText = "One Billion Two Hundred and Twenty-Three Million Four Hundred and Sixty-Three Thousand Seven Hundred and Eighteen";
            Assert.AreEqual(expectedText, NumberUtility.ConvertNumberToText(1223463718));
        }

    }
}

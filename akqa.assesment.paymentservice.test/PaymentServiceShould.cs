﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace akqa.assesment.paymentservice.test
{
    [TestClass]
    public class PaymentServiceShould
    {
        [TestMethod]
        public void SaveAPersonsDetails()
        {
            var service = new paymentservice.PaymentService();

            var personPayment = service.SavePayment("John", "Crawford", 1234567.89);

            Assert.AreEqual("John", personPayment.FirstName);
            Assert.AreEqual("Crawford", personPayment.LastName);
            Assert.AreEqual(1234567.89, personPayment.PaymentAmount);
        }

        [TestMethod]
        public void ThrowExceptionWhenMissingNameProperties()
        {
            var service = new paymentservice.PaymentService();

            // These types of tests are effective the code coverage, so changed to get the numbers higher
            //Assert.ThrowsException<InvalidOperationException>(() => service.SavePersonPayment(null, "Crawford", 1234567.89));
            //Assert.ThrowsException<InvalidOperationException>(() => service.SavePersonPayment("John", null, 1234567.89));
            //Assert.ThrowsException<InvalidOperationException>(() => service.SavePersonPayment(null, null, 1234567.89));

            try
            {
                service.SavePayment(null, "Crawford", 1234567.89);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsNotNull(ex);
                
            }

            try
            {
                service.SavePayment("John", null, 1234567.89);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsNotNull(ex);

            }

            try
            {
                service.SavePayment(null, null, 1234567.89);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        [TestMethod]
        public void CalculateCorrectTextDisplayValue()
        {
            var service = new paymentservice.PaymentService();

            var personPayment = service.SavePayment("John", "Crawford", 1234567.89);

            Assert.AreEqual("One Million Two Hundred and Thirty-Four Thousand Five Hundred and Sixty-Seven Dollars and Eighty-Nine Cents", personPayment.PaymentDisplayValue);
        }
    }
}

﻿using akqa.assesment.business.Model;
using System;
using System.Linq;

namespace akqa.assesment.business.Data
{
    /// <summary>
    /// Management class to encapsulate the interface to the 
    /// Akqa database
    /// </summary>
    public class AkqaRepository
    {
        /// <summary>
        /// Persists a new payment to the database
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="paymentAmount"></param>
        /// <returns></returns>
        public static int InsertPayment(string firstName, string lastName, double paymentAmount)
        {
            using (var context = new Model.Akqa())
            {
                var payment = new Payment()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    PaymentAmount = paymentAmount,
                    DateCreated = DateTime.Now
                };

                context.Payments.Add(payment);
                int id = context.SaveChanges();
                return id;
            }
        }

        /// <summary>
        /// Returns a count of the number of payments
        /// used in testing
        /// </summary>
        /// <returns></returns>
        public static int GetNumberOfPayments()
        {
            using (var context = new Model.Akqa())
            {
                return context.Payments.Count();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.business.Utilities
{
    public class NumberUtility
    {
       

        /// <summary>
        /// returns a textual representation of the amount in full dollars and cents 
        /// text notation
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="format">Control the output format of the generated text</param>
        /// <returns></returns>
        public static string ConvertAmountToCurrencyText(double amount, TextFormat format = TextFormat.TitleCase)
        {
            if(amount == 0)
            {
                return "Zero Dollars and Zero Cents";
            }
            
            string text = string.Empty;
            if(amount < 0)
            {
                text = "(Minus) ";
                amount = Math.Abs(amount);
            }

            // split into dollars and cents
            // and use the recursive ConvertNumber to Text to create the 
            // dollars part and the cents part of the 
            int cents = (int)Math.Round(((amount - Math.Truncate(amount)) * 100));
            int dollars = (int)amount;

            if (dollars == 0) {
                text += "Zero Dollars ";
            }
            else
            {
                text += ConvertNumberToText(dollars) + " Dollar" + ((dollars == 1) ? " " : "s ");
            }

            if (text != "")
                text += "and ";
            
             text += ConvertNumberToText(cents) + " Cent" + ((cents == 1) ? "" : "s");

            // Conditional formatting 
            switch (format)
            {
                case TextFormat.Lowercase:
                    text = text.ToLower();
                    break;
                case TextFormat.Uppercase:
                    text = text.ToUpper();
                    break;
            }
            
            return text;

        }

        /// <summary>
        /// Returns any whole number up to billions as a text representation of the number
        /// Handles Negative numbers with preceeding (Minus)
        /// Returns in title case
        /// </summary>
        /// <param name="number">Work with whole numbers</param>
        /// <returns></returns>
        public static string ConvertNumberToText(int number)
        {
            if (number == 0)
            {
                return "Zero";
            }

            if (number <= 0)
            {
                return "(Minus)" + ConvertNumberToText(Math.Abs(number));
            }

            string text = "";

            if ((number / 1000000000) > 0)
            {
                text += ConvertNumberToText(number / 1000000000) + " Billion ";
                number %= 1000000000;
            }

            if ((number / 1000000) > 0)
            {
                text += ConvertNumberToText(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                text += ConvertNumberToText(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                text += ConvertNumberToText(number / 100) + " Hundred ";
                number %= 100;
            }


            if (number > 0)
            {
                if (text != "")
                    text += "and ";

                if (number >= 1)
                {
                    string[] unitsWords = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                    string[] tensOfWords = new[] { "Zero", "Ten", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                    if (number < 20)
                        text += unitsWords[(int)number];
                    else
                    {
                        text += tensOfWords[(int)number / 10];
                        if ((number % 10) > 0)
                            text += "-" + unitsWords[(int)number % 10];
                    }

                }
            }

            // ensure no trailing white space on text
            return text.TrimEnd();
        }
    }
}
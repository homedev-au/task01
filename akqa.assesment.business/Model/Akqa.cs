namespace akqa.assesment.business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class Akqa : DbContext
    {
        // Your context has been configured to use a 'Akqa' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'akqa.assesment.business.Model.Akqa' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Akqa' 
        // connection string in the application configuration file.
        public Akqa()
            : base("name=AkqaDb")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Payment> Payments { get; set; }
    }

    public class Payment
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double PaymentAmount { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
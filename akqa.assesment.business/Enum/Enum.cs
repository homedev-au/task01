﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace akqa.assesment.business
{
    /// <summary>
    /// Text Format Enumeration used to give the ability to alter the output style
    /// of the convert to currently
    /// </summary>
    public enum TextFormat
    {
        Uppercase,
        Lowercase,
        TitleCase
    }
}
